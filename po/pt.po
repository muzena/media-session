#
# Rui Gouveia <rui.gouveia@globaltek.pt>, 2012.
# Rui Gouveia <rui.gouveia@globaltek.pt>, 2012, 2015.
# Pedro Albuquerque <palbuquerque73@openmailbox.com>, 2015.
# Juliano de Souza Camargo <julianosc@pm.me>, 2020.
# Hugo Carvalho <hugokarvalho@hotmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: pipewire\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pipewire/pipewire-media-"
"session/issues/new\n"
"POT-Creation-Date: 2021-10-20 10:03+1000\n"
"PO-Revision-Date: 2021-08-02 15:28+0100\n"
"Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>\n"
"Language-Team: Portuguese <https://l10n.gnome.org/teams/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.0\n"

#: src/alsa-monitor.c:662
msgid "Built-in Audio"
msgstr "Áudio interno"

#: src/alsa-monitor.c:666
msgid "Modem"
msgstr "Modem"

#: src/alsa-monitor.c:675
msgid "Unknown device"
msgstr "Dispositivo desconhecido"
