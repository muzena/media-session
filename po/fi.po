# pipewire translation to Finnish (fi).
# Copyright (C) 2008 Timo Jyrinki
# This file is distributed under the same license as the pipewire package.
# Timo Jyrinki <timo.jyrinki@iki.fi>, 2008.
# Ville-Pekka Vainio <vpivaini@cs.helsinki.fi>, 2009, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: git trunk\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pipewire/pipewire-media-"
"session/issues/new\n"
"POT-Creation-Date: 2021-10-20 10:03+1000\n"
"PO-Revision-Date: 2021-04-18 16:38+0300\n"
"Last-Translator: Ricky Tigg <ricky.tigg@gmail.com>\n"
"Language-Team: Finnish <https://translate.fedoraproject.org/projects/"
"pipewire/pipewire/fi/>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.5.1\n"

#: src/alsa-monitor.c:662
msgid "Built-in Audio"
msgstr "Sisäinen äänentoisto"

#: src/alsa-monitor.c:666
msgid "Modem"
msgstr "Modeemi"

#: src/alsa-monitor.c:675
msgid "Unknown device"
msgstr "Tuntematon laite"
