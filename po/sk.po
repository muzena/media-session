# Slovak translation for PipeWire.
# Copyright (C) 2014 PipeWire's COPYRIGHT HOLDER
# This file is distributed under the same license as the PipeWire package.
# Dušan Kazik <prescott66@gmail.com>, 2014, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: PipeWire master\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pipewire/pipewire-media-"
"session/issues/new\n"
"POT-Creation-Date: 2021-10-20 10:03+1000\n"
"PO-Revision-Date: 2020-11-25 08:35+0000\n"
"Last-Translator: Dusan Kazik <prescott66@gmail.com>\n"
"Language-Team: Slovak <https://translate.fedoraproject.org/projects/pipewire/"
"pipewire/sk/>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0;\n"
"X-Generator: Weblate 4.3.2\n"

#: src/alsa-monitor.c:662
msgid "Built-in Audio"
msgstr "Vstavaný zvuk"

#: src/alsa-monitor.c:666
msgid "Modem"
msgstr "Modem"

#: src/alsa-monitor.c:675
msgid "Unknown device"
msgstr ""
